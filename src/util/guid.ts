export class GuidUtil {
  public static isValidGuid(guid: any) {
    if (
      guid !== null &&
      typeof guid === 'string' &&
      guid.match(/[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}/i) &&
      guid !== '00000000-0000-0000-0000-000000000000'
    ) {
      return true;
    }
    return false;
  }

  public static getResourceId() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('resource_id')) {
      const urlResourceId = urlParams.get('resource_id');
      if (this.isValidGuid(urlResourceId)) {
        return urlResourceId;
      }
    }
    return '';
  }

  public static getPIDSuffix() {
    let pid = GuidUtil.getPID();
    if (pid.lastIndexOf('/') !== -1) {
      return pid.split('/')[1];
    }
    return '';
  }

  public static getPID() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('pid')) {
      let urlPID = decodeURIComponent(urlParams.get('pid') as string);
      if (urlPID.lastIndexOf('http://hdl.handle.net/') !== -1) {
        return urlPID.split('http://hdl.handle.net/')[1];
      }
    }
    return '';
  }

  public static getProjectId() {
    return coscine.project.id;
  }

  public static getUrlParam(param: string) {
    let searchParam = new URLSearchParams(window.location.search);
    return searchParam.get(param);
  }
}
