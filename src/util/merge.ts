export class MergeUtil {
  public static merge(obj1: any, obj2: any) {
    if (obj1 === undefined) {
      obj1 = {};
    }
    for (const prop in obj2) {
      if (obj2.hasOwnProperty(prop)) {
        if (Object.prototype.toString.call(obj2[prop]) === '[object Object]') {
          obj1[prop] = this.merge(obj1[prop], obj2[prop]);
        } else {
          obj1[prop] = obj2[prop];
        }
      }
    }
    return obj1;
  }
}
