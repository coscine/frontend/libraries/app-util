export class LanguageUtil {
  public static getLanguage() {
    if (!coscine || !coscine.language || !coscine.language.locale) {
      return 'en';
    }
    switch (coscine.language.locale) {
      case 'de':
        return coscine.language.locale;
      default:
        return 'en';
    }
  }
}
