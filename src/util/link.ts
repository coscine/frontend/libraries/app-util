export class LinkUtil {
  public static getProjectSlug() {
    const baseUrl = _spPageContextInfo.siteAbsoluteUrl;
    const splitUrl = baseUrl.split('/');
    const projectUri = splitUrl[splitUrl.length - 1];
    return projectUri;
  }

  public static getHostName() {
    let hostName = window.location.hostname;
    if (hostName.indexOf(':') !== -1) {
      if (hostName.indexOf('https://') !== -1) {
        hostName = hostName.replace('https://', '');
      }
      hostName = hostName.substr(0, hostName.indexOf(':'));
    }
    return hostName;
  }

  public static getRootLink(page: string = 'Home', queryParams: {} = {}, hashTag: string = '') {
    return (
      this.formatLink('', page) + this.formatQueryParams(queryParams) + this.formatHashTag(hashTag)
    );
  }

  public static getProjectLink(page: string = 'Home', queryParams: {} = {}, hashTag: string = '') {
    return (
      this.formatLink(this.getProjectSlug(), page) +
      this.formatQueryParams(queryParams) +
      this.formatHashTag(hashTag)
    );
  }

  public static getExternalProjectLink(
    projectSlug: string = this.getProjectSlug(),
    page: string = 'Home',
    queryParams: {} = {},
    hashTag: string = ''
  ) {
    return (
      this.formatLink(projectSlug, page) +
      this.formatQueryParams(queryParams) +
      this.formatHashTag(hashTag)
    );
  }

  public static redirectToRoot(page: string = 'Home', queryParams: {} = {}, hashTag: string = '') {
    window.location.href = this.getRootLink(page, queryParams, hashTag);
  }
  public static redirectToProject(
    page: string = 'Home',
    queryParams: {} = {},
    hashTag: string = ''
  ) {
    window.location.href = this.getProjectLink(page, queryParams, hashTag);
  }

  public static redirectToExternalProject(
    projectSlug: string = this.getProjectSlug(),
    page: string = 'Home',
    queryParams: {} = {},
    hashTag: string = ''
  ) {
    window.location.href = this.getExternalProjectLink(projectSlug, page, queryParams, hashTag);
  }

  public static getScriptPath(pathToAppJs: string) {
    const scriptUrl =
      pathToAppJs === '' ? '/' : pathToAppJs.substring(0, pathToAppJs.indexOf('app.js'));
    const rootUrl = pathToAppJs.indexOf('/js') !== -1 ? scriptUrl.replace('/js', '') : scriptUrl;
    return rootUrl;
  }

  private static formatQueryParams(queryParams: any) {
    let queryString = '';
    if (queryParams !== null && queryParams !== undefined) {
      let prop = Object.getOwnPropertyNames(queryParams);
      if (prop.length > 0) {
        for (let i = 0; i < prop.length; i++) {
          if (i === 0) {
            queryString += '?';
          } else {
            queryString += '&';
          }
          queryString += prop[i] + '=' + queryParams[prop[i]];
        }
      }
    }
    return queryString;
  }

  private static formatHashTag(hashTag: string) {
    let addToLink = '';
    if (hashTag !== '') {
      addToLink = '#' + hashTag;
    }
    return addToLink;
  }

  private static formatLink(projectSlug: string = '', page: string = 'Home') {
    let projectLinkPart = '';
    if (projectSlug !== '') {
      projectLinkPart += 'p/' + projectSlug.toLowerCase() + '/';
    }
    let pageLinkPart = '';
    if (page.indexOf('/') === -1) {
      pageLinkPart += 'SitePages/';
    }
    pageLinkPart += page + '.aspx';

    return 'https://' + this.getHostName() + '/' + projectLinkPart + pageLinkPart;
  }
}
