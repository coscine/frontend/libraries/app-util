declare var coscine: {
  authorization: {
    bearer: string;
  };
  language: {
    locale: string;
  };
  project: {
    id: string;
  };
};

declare var _spPageContextInfo: any;
