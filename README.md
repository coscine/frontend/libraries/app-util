## App-Util library

This library provides the app-util functionality for all CoScInE Typescript applications.

Currently it includes functionality for detecting the language change for a vue application.